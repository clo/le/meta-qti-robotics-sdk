# Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

## Script to do pre-configurations specific to current layer

#!/bin/bash
set -x
#the layers to add
current_layers="poky/meta-qti-robotics/"
BBLAYER_CONF=$BUILDDIR/conf/bblayers.conf
Install_pkgs="packagegroup-qti-robotics"

if [[ $OE_SKIP_SDK_CHECK =~ "1" ]]; then
    for layer in ${current_layers}; do
        if [[ "$(cat $BBLAYER_CONF)" =~ "$layer" ]];  then
            echo "$layer has added in bblayers.conf"
        else
            echo "add new layer to bblayers"
            echo "BBLAYERS += \" \${SDKBASEMETAPATH}/layers/$layer \"" >> $BBLAYER_CONF
        fi
    done
fi
