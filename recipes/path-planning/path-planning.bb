DESCRIPTION = "path-planning module"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/BSD;md5=3775480a712fc46a69647678acb234cb"
LICENSE = "BSD"
inherit cmake

PR = "r0"
PV = "1.0"

S = "${WORKDIR}/vendor/qcom/opensource/robotics-oss/navigation-stack/path-planning"
FILESPATH =+ "${TOPDIR}/layers/src:"
SRC_URI = "file://vendor/qcom/opensource/robotics-oss/navigation-stack/path-planning/"

FILES_${PN} += "/data/pathplan/*"

PACKAGES = "${PN}"

do_install() {
    dest=/data/pathplan/
    install -d ${D}${dest}
    cp -rf ${S}/* ${D}${dest}
}
