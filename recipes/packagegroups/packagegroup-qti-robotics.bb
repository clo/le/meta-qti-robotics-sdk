SUMMARY = "QTI Robotics opensource Package Group"

LICENSE = "BSD-3-Clause"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS_packagegroup-qti-robotics = ' \
    librealsense2 \
    path-planning \
    vslam \
    follow-me \
'
